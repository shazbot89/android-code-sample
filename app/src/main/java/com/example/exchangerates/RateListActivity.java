package com.example.exchangerates;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.example.exchangerates.api.fixer.FixerApi;
import com.example.exchangerates.api.fixer.FixerResponse;
import com.example.exchangerates.api.fixer.Rate;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import javax.inject.Inject;

/**
 * An activity representing a list of Rates. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link RateDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class RateListActivity extends AppCompatActivity
        implements SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = RateListActivity.class.getSimpleName();
    @Inject FixerApi mApi;
    @BindView(R.id.rate_list)
    RecyclerView mRecyclerView;
    @BindView(R.id.rate_list_swipe_refresh)
    SwipeRefreshLayout mSwipeLayout;

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        App.getComponent(this).inject(this);
        setContentView(R.layout.activity_rate_list);
        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> Snackbar.make(view, "You wanna email?", Snackbar.LENGTH_LONG)
                .setAction("Yasss", v -> email()).show());

        if (findViewById(R.id.rate_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }

        mRecyclerView.setAdapter(null);
        mSwipeLayout.setOnRefreshListener(this);

        fetchRates();
    }

    private void email() {
        Toast.makeText(this, "Email!", Toast.LENGTH_SHORT).show();
    }

    /**
     * Calls API for Exchange Rates
     */
    private void fetchRates() {
        mApi.listRates()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        response -> {
                            mRecyclerView.setAdapter(new SimpleItemRecyclerViewAdapter(response));
                            mSwipeLayout.setRefreshing(false);
                        },
                        throwable -> {
                            Log.e(TAG, "fetchRates: ", throwable);
                            mSwipeLayout.setRefreshing(false);
                            Toast.makeText(this, "Unable to update rates :c ", Toast.LENGTH_LONG).show();
                        }
                );
    }

    @Override
    public void onRefresh() {
        fetchRates();
    }

    public class SimpleItemRecyclerViewAdapter
            extends RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder> {

        private final FixerResponse mResponse;

        SimpleItemRecyclerViewAdapter(FixerResponse response) {
            mResponse = response;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.rate_list_content, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            Rate rate = mResponse.getRateList().get(position);

            holder.mItem = rate;
            holder.mIdView.setText(rate.getSymbol());
            holder.mContentView.setText(String.valueOf(rate.getRate()));
            holder.mResponse = mResponse;

            holder.mView.setOnClickListener(v -> {
                if (mTwoPane) {
                    Bundle arguments = new Bundle();
                    arguments.putSerializable(RateDetailFragment.ARG_RATE_ID, holder.mItem);
                    arguments.putSerializable(RateDetailFragment.ARG_RATE_CONTEXT, holder.mResponse);
                    RateDetailFragment fragment = new RateDetailFragment();
                    fragment.setArguments(arguments);
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.rate_detail_container, fragment)
                            .commit();
                } else {
                    Context context = v.getContext();
                    Intent intent = new Intent(context, RateDetailActivity.class);
                    intent.putExtra(RateDetailFragment.ARG_RATE_ID, holder.mItem);
                    intent.putExtra(RateDetailFragment.ARG_RATE_CONTEXT, holder.mResponse);

                    context.startActivity(intent);
                }
            });
        }

        @Override
        public int getItemCount() {
            return mResponse.getRates().size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            final View mView;
            @BindView(R.id.id) TextView mIdView;
            @BindView(R.id.content) TextView mContentView;
            Rate mItem;
            FixerResponse mResponse;

            ViewHolder(View view) {
                super(view);
                ButterKnife.bind(this, view);
                mView = view;
            }
        }
    }
}
