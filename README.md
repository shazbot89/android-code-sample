# Android Coding Sample

### Overview
A quick sample app in native Android. Pulls exchange rates for the Euro. 
Caching results from an active connection for 1 minute, saving for a day if no connection available. 

### GitLab Project
https://gitlab.com/shazbot89/android-code-sample

### Workaround: Possible IDE Issue with IntelliJ
If you use IntelliJ, there seems to be an issue when running for the first time where instant run has no UID for app to swap out.  
**Disable Instant Run:** [https://stackoverflow.com/a/43365585/3645949](https://stackoverflow.com/a/43365585/3645949)

### Libraries Used
* Retrofit - http://square.github.io/retrofit/
* RxJava - https://github.com/ReactiveX/RxJava
* Dagger - https://google.github.io/dagger/
* ButterKnife - http://jakewharton.github.io/butterknife/