package com.example.exchangerates.api.fixer;

import java.io.Serializable;

/**
 * Created by shaunburch on 3/19/18.
 */

public class Rate implements Serializable {
    private String symbol;
    private double rate;

    public Rate(String symbol, double rate) {
        this.symbol = symbol;
        this.rate = rate;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }
}
