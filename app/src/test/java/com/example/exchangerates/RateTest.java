package com.example.exchangerates;

import com.example.exchangerates.api.fixer.Rate;
import org.junit.Test;

import static org.junit.Assert.*;

public class RateTest {
    // TODO: 5/17/18 These might actually be better in instrumented test to monitor for API changes

    private Rate rateObj = new Rate("USD", 0.806942);
    private String ratePattern = "^([0-9]*)[.]?([0-9]{0,6})$";

    @Test
    public void getSymbol() {
        // No mutation
        assertSame("USD", rateObj.getSymbol());
        // Only three capital letters
        assertTrue(rateObj.getSymbol().matches("^[A-Z]{3}$"));
    }

    @Test
    public void setSymbol() {
        // No mutation
        rateObj.setSymbol("EUR");
        assertSame("EUR", rateObj.getSymbol());
    }

    @Test
    public void getRate() {
        double rate = this.rateObj.getRate();
        // No mutation
        assertEquals(0.806942, rate, 0.0);
        // Double amount pattern
        assertTrue(String.valueOf(rate).matches(ratePattern));
    }

    @Test
    public void setRate() {
        rateObj.setRate(1);
        // No mutation
        assertEquals(1.0, rateObj.getRate(), 0.0);
        // Whole # is ok
        assertTrue(String.valueOf(rateObj.getRate()).matches(ratePattern));
    }
}