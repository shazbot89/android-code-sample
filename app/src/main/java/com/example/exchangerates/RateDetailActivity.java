package com.example.exchangerates;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import com.example.exchangerates.api.fixer.FixerResponse;
import com.example.exchangerates.api.fixer.Rate;

/**
 * An activity representing a single Rate detail screen. This
 * activity is only used narrow width devices. On tablet-size devices,
 * item details are presented side-by-side with a list of items
 * in a {@link RateListActivity}.
 */
public class RateDetailActivity extends AppCompatActivity {

    private Rate mRate;
    private FixerResponse mRateContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.getComponent(this).inject(this);
        setContentView(R.layout.activity_rate_detail);
        Toolbar toolbar = findViewById(R.id.detail_toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view ->
                Snackbar
                .make(view, "Share this neat news?", Snackbar.LENGTH_LONG)
                .setAction("Share", v -> onShareTapped())
                .show()
        );

        // Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        // savedInstanceState is non-null when there is fragment state
        // saved from previous configurations of this activity
        // (e.g. when rotating the screen from portrait to landscape).
        // In this case, the fragment will automatically be re-added
        // to its container so we don't need to manually add it.
        // For more information, see the Fragments API guide at:
        //
        // http://developer.android.com/guide/components/fragments.html
        //
        if (savedInstanceState == null) {
            // Create the detail fragment and add it to the activity
            // using a fragment transaction.
            Bundle arguments = new Bundle();
            Intent intent = getIntent();

            mRate = (Rate) intent.getSerializableExtra(RateDetailFragment.ARG_RATE_ID);
            mRateContext = (FixerResponse) intent.getSerializableExtra(RateDetailFragment.ARG_RATE_CONTEXT);

            arguments.putSerializable(RateDetailFragment.ARG_RATE_ID, mRate);
            arguments.putSerializable(RateDetailFragment.ARG_RATE_CONTEXT, mRateContext);

            RateDetailFragment fragment = new RateDetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.rate_detail_container, fragment)
                    .commit();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(RateDetailFragment.ARG_RATE_ID, mRate);
        outState.putSerializable(RateDetailFragment.ARG_RATE_CONTEXT, mRateContext);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        mRate = (Rate) savedInstanceState.getSerializable(RateDetailFragment.ARG_RATE_ID);
        mRateContext = (FixerResponse) savedInstanceState.getSerializable(RateDetailFragment.ARG_RATE_CONTEXT);
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //
            navigateUpTo(new Intent(this, RateListActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void onShareTapped() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        String shareMessage = String.format(
                getString(R.string.share_message),
                mRateContext.getBase(),
                mRate.getRate(),
                mRate.getSymbol()
        );
        sendIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }
}
