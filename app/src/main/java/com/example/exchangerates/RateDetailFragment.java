package com.example.exchangerates;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.example.exchangerates.api.fixer.FixerResponse;
import com.example.exchangerates.api.fixer.Rate;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * A fragment representing a single Rate detail screen.
 * This fragment is either contained in a {@link RateListActivity}
 * in two-pane mode (on tablets) or a {@link RateDetailActivity}
 * on handsets.
 */
public class RateDetailFragment extends Fragment {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_RATE_ID = "RATE_ID";
    public static final String ARG_RATE_CONTEXT = "RATE_CONTEXT";

    private Rate mRate;
    private FixerResponse mRateContext;
    private Context mContext;

    @BindView(R.id.symbol_to) TextView mSymbolTo;
    @BindView(R.id.symbol_base) TextView mSymbolBase;
    @BindView(R.id.rate_to) TextView mRateTo;
    @BindView(R.id.retrieved_at) TextView mRetrievedAt;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public RateDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this.getActivity();
        App.getComponent(mContext).inject(this);
        Bundle arguments = getArguments();

        // TODO: 5/17/18 null check Args
        if (arguments.containsKey(ARG_RATE_ID) && arguments.containsKey(ARG_RATE_CONTEXT)) {
            mRate = (Rate) arguments.getSerializable(ARG_RATE_ID);
            mRateContext = (FixerResponse) arguments.getSerializable(ARG_RATE_CONTEXT);
        }

        // Set toolbar title
        Activity activity = this.getActivity();
        CollapsingToolbarLayout appBarLayout = activity.findViewById(R.id.toolbar_layout);
        if (appBarLayout != null) {
            appBarLayout.setTitle(String.format(getString(R.string.euro_to_cur), mRate.getSymbol()));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.rate_detail, container, false);
        ButterKnife.bind(this, rootView);

        if (mRate != null && mRateContext != null) {
            // Set Symbols
            mSymbolBase.setText(mRateContext.getBase());
            mSymbolTo.setText(mRate.getSymbol());

            // Set Rate
            mRateTo.setText(String.valueOf(mRate.getRate()));

            // Set retrieval timestamp
            Date date = new Date(mRateContext.getTimestamp() * 1000L);
            DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, Locale.getDefault());
            mRetrievedAt.setText(String.format("Retrieved %1s", dateFormat.format(date)));
        }

        return rootView;
    }
}
