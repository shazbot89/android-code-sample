package com.example.exchangerates.api.fixer;

import io.reactivex.Single;
import retrofit2.http.GET;

/**
 * Retrofit Interface for the Fixer.io exchange rates API.
 * <p>
 * Doc: https://fixer.io/documentation
 */
public interface FixerApi {

    String VERSION = "/latest";

    /**
     * Get a list of exchange rates
     *
     * @return Call with list of exchange rates
     */
    @GET("/api" + VERSION)
    Single<FixerResponse> listRates();

}
