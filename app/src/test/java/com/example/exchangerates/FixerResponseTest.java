package com.example.exchangerates;

import com.example.exchangerates.api.fixer.FixerResponse;
import com.example.exchangerates.api.fixer.Rate;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;

public class FixerResponseTest {

    private FixerResponse fixerResponseObj = new FixerResponse();

    @Before
    public void setUp() {
        Map<String, Double> rateMap = new HashMap<>();
        rateMap.put("AUD", 1.566015);
        rateMap.put("CAD", 1.560132);
        rateMap.put("CHF", 1.154727);
        rateMap.put("CNY", 7.827874);
        rateMap.put("GBP", 0.882047);
        rateMap.put("JPY", 132.360679);
        rateMap.put("USD", 1.23396);
        fixerResponseObj.setRates(rateMap);
    }

    @Test
    public void getRateList() {
        List<Rate> rateList = fixerResponseObj.getRateList();
        // Is a list
        assertThat(rateList, instanceOf(List.class));
        // of Rates
        assertThat(rateList.get(0), instanceOf(Rate.class));
    }

}