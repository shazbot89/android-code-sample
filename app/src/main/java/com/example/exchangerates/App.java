package com.example.exchangerates;

import android.app.Application;
import android.content.Context;
import com.example.exchangerates.di.ApiModule;
import com.example.exchangerates.di.ApplicationComponent;
import com.example.exchangerates.di.DaggerApplicationComponent;

public class App extends Application {
    private ApplicationComponent mAppComponent;

    public static ApplicationComponent getComponent(Context context) {
        return ((App) context.getApplicationContext()).mAppComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mAppComponent = DaggerApplicationComponent
                .builder()
                .apiModule(new ApiModule(this, BuildConfig.API_FIXER, BuildConfig.API_FIXER_ACCESS_KEY))
                .build();
    }

}
