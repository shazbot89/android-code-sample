package com.example.exchangerates.di;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.example.exchangerates.api.fixer.FixerApi;
import dagger.Module;
import dagger.Provides;
import okhttp3.*;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import javax.inject.Singleton;

@Module
public class ApiModule {

    private final Context mContext;
    private final String mApiUrl;
    private final String mApiKey;

    public ApiModule(Context context, String url, String apiKey) {
        this.mContext = context;
        this.mApiUrl = url;
        this.mApiKey = apiKey;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return mContext;
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(Cache cache, ConnectivityManager cm) {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        // Add Caching
        httpClient.cache(cache);

        // Intercept: Add API Access Key
        httpClient.addInterceptor(chain -> {
            Request original = chain.request();
            HttpUrl originalHttpUrl = original.url();
            HttpUrl url = originalHttpUrl.newBuilder()
                    .addQueryParameter("access_key", mApiKey)
                    .build();

            // Request customization: add request headers
            Request.Builder requestBuilder = original.newBuilder().url(url);
            // Fixes issue with 304, Unexpected status line
            requestBuilder.addHeader("Connection", "close");

            Request request = requestBuilder.build();
            return chain.proceed(request);
        });

        httpClient.addNetworkInterceptor(chain -> {
            Response originalResponse = chain.proceed(chain.request());
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            // If active, cache for a minute
            if (activeNetwork != null && activeNetwork.isConnectedOrConnecting()) {
                int maxAge = 60; // read from cache for 1 minute
                return originalResponse.newBuilder()
                        .header("Cache-Control", "public, max-age=" + maxAge)
                        .build();
            } else { // otherwise, cache up to a day
                int maxStale = 60 * 60 * 24; // tolerate 4-weeks stale
                return originalResponse.newBuilder()
                        .header("Cache-Control", "public, only-if-cached, max-stale=" + maxStale)
                        .build();
            }
        });

        return httpClient.build();
    }

    @Provides
    @Singleton
    Cache provideHttpCache(Context context){
        // Add Caching
        int cacheSize = 10 * 1024 * 1024; // 10 MiB
        return new Cache(context.getCacheDir(), cacheSize);
    }

    @Provides
    @Singleton
    FixerApi provideFixerService(OkHttpClient client) {
        return new Retrofit.Builder()
                .baseUrl(mApiUrl)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(FixerApi.class);
    }

    @Provides
    @Singleton
    ConnectivityManager provideConnectivty(Context context){
        return (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

}
