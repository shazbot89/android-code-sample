package com.example.exchangerates.api.fixer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by shaunburch on 3/19/18.
 */

public class FixerResponse implements Serializable {
    private boolean success;
    private long timestamp;
    private String base;
    private Date date;
    private Map<String, Double> rates;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Map<String, Double> getRates() {
        return rates;
    }

    public void setRates(Map<String, Double> rates) {
        this.rates = rates;
    }

    public List<Rate> getRateList() {
        ArrayList<Rate> rateList = new ArrayList<>();

        for (Map.Entry<String, Double> entry : rates.entrySet()) {
            rateList.add(new Rate(entry.getKey(), entry.getValue()));
        }

        return rateList;
    }

    @Override
    public String toString() {
        return "FixerResponse{" +
                "success=" + success +
                ", timestamp=" + timestamp +
                ", base='" + base + '\'' +
                ", date=" + date +
                ", rates=" + rates +
                '}';
    }
}
