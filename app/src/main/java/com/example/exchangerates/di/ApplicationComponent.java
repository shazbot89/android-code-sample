package com.example.exchangerates.di;

import com.example.exchangerates.RateDetailActivity;
import com.example.exchangerates.RateDetailFragment;
import com.example.exchangerates.RateListActivity;
import dagger.Component;

import javax.inject.Singleton;

@Singleton
@Component(modules = ApiModule.class)
public interface ApplicationComponent {

    void inject(RateListActivity rateListActivity);

    void inject(RateDetailFragment rateDetailFragment);

    void inject(RateDetailActivity rateDetailActivity);

}